package com.example.testapp;

public class ArduinoComm extends CommClass {
    private static final int ID = 0;

    public ArduinoComm(MainActivity ma){
        super(ma);
    }

    //MODIFIES: this
    //EFFECTS: process action to connect to Arduino,
    // must update IsConnected Accordingly
    public void processConnectAction(){
        mainActivity.setArduinoConnected(!isConnected);
        isConnected = !isConnected;
        mainActivity.appendArduino("Hey There!");
    }

    //MODIFIES: this
    //EFFECTS: changes update rate to specified updates per seconds
    public void setUpdateRate(int updatesPerSecond){
        mainActivity.appendArduino("Updates: " + updatesPerSecond + " per second");
    }
}