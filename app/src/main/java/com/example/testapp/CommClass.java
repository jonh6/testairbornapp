package com.example.testapp;


import android.app.Activity;

public abstract class CommClass {
    protected  MainActivity mainActivity;
    protected boolean isConnected;

    private static int ARDUINO_CALLER = 0;
    private static int SERVER_CALLER = 1;

    public CommClass(MainActivity mainActivity){
        this.mainActivity = mainActivity;
        isConnected = false;
    }


    public abstract void processConnectAction();

    public  abstract void setUpdateRate(int updatesPerSecond);

    public boolean isConnected(){
        return isConnected;
    }

    protected void appendUiThread(final String text, final int ID){
        mainActivity.runOnUiThread(new Runnable(){
            public void run(){
               if (ID == ARDUINO_CALLER){
                   mainActivity.appendArduino(text);
               }
               else if (ID == SERVER_CALLER){
                   mainActivity.appendServer(text);
               }
            }
        });
    }

    protected void updateConnectionStatus(final int ID){
        mainActivity.runOnUiThread(new Runnable(){
            public void run(){
                if (ID == ARDUINO_CALLER){
                    mainActivity.setServerConnected(isConnected);
                }
                else if (ID == SERVER_CALLER){
                   mainActivity.setServerConnected(isConnected);
                }
            }
        });
    }



}
