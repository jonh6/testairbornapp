package com.example.testapp;

        import androidx.appcompat.app.AppCompatActivity;

        import android.os.Bundle;
        import android.view.View;
        import android.view.WindowManager;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.Spinner;
        import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String CONNECTED = "Connected";
    private static final String NOT_CONNECTED = "Not Connected";
    public static final String NEW_LINE = "\n";

    private TextView arduinoView;
    private TextView serverView;

    private Button arduinoButton;
    private Button serverButton;
    private CommClass arduinoComm;
    private CommClass serverComm;

    private TextView arduinoStatus;
    private TextView serverStatus;

    private Spinner arduinoSpinner;
    private Spinner serverSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serverComm = new ServerComm(this);
        arduinoComm = new ArduinoComm(this);

        arduinoView = (TextView) findViewById(R.id.arduinoView);
        serverView = (TextView) findViewById(R.id.serverView);

        arduinoStatus = (TextView) findViewById(R.id.arduinoStatus);
        serverStatus = (TextView) findViewById(R.id.serverStatus);


        Integer [] items = new Integer[] {1,2,3,4,5,6,7,8,9,10};

        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, items);

        serverSpinner = (Spinner) findViewById(R.id.serverSpinner);
        arduinoSpinner = (Spinner) findViewById(R.id.arduinoSpinner);

        serverSpinner.setAdapter(adapter);
        arduinoSpinner.setAdapter(adapter);
        arduinoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                arduinoComm.setUpdateRate((int)parent.getSelectedItem());
            }

        });

        setServerConnected(false);
        setArduinoConnected(false);

        arduinoButton = (Button) findViewById(R.id.connectArduino);
        arduinoButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                arduinoComm.processConnectAction();
            }
        });

        serverButton = (Button) findViewById(R.id.connectServer);
        serverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serverComm.processConnectAction();
            }
        });

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void appendToView(String text, TextView textView){
        textView.setText(text + NEW_LINE + textView.getText());
    }

    private void setConnectionStatus(boolean connected, TextView textView) {
        textView.setText(connected ? CONNECTED : NOT_CONNECTED);
    }

    public void setArduinoConnected(boolean connected){
        setConnectionStatus(connected, arduinoStatus);
        appendArduino(connected ? CONNECTED : NOT_CONNECTED);
    }

    public void setServerConnected(boolean connected){
        setConnectionStatus(connected, serverStatus);
        appendServer(connected ? CONNECTED : NOT_CONNECTED);
    }

    public void appendArduino(String text){
        appendToView(text, arduinoView);
    }

    public void appendServer(String text){
        appendToView(text, serverView);
    }
}
