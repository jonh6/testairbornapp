package com.example.testapp;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;

import tech.gusavila92.websocketclient.WebSocketClient;

public class ServerComm extends CommClass {


    private static final String WEB_SERVER_IP_PORT = "ws://104.248.73.89:8888";
    private static final int ID = 1;


    private android.os.Handler customHandler;
    private LocationManager locationManager;

    private WebSocketClient webSocketClient;
    private Thread connectionThread = null;
    private MyLocationListener locationListener;
    private boolean isRunning = false;

    public ServerComm(MainActivity mainActivity){
        super(mainActivity);


        if ( ContextCompat.checkSelfPermission(mainActivity, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {

            ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.ACCESS_FINE_LOCATION  },
                    MyLocationListener.MY_PERMISSION_ACCESS_FINE_LOCATION);
        }
    }


    //MODIFIES: this
    //EFFECTS: process action to connect to Server,
    // must update IsConnected Accordingly
    public void processConnectAction(){
        requestLocationUpdates();
    }


    //MODIFIES: this
    //EFFECTS: changes update rate to specified updates per seconds
    public  void setUpdateRate(int updatesPerSecond){ }


    public void requestLocationUpdates(){
        if ( ContextCompat.checkSelfPermission(mainActivity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {

            mainActivity.appendServer("Location Permission not Granted");
            ActivityCompat.requestPermissions( mainActivity, new String[] {  android.Manifest.permission.ACCESS_FINE_LOCATION  },
                    MyLocationListener.MY_PERMISSION_ACCESS_FINE_LOCATION);
        }
        else {
            locationManager = (LocationManager) mainActivity.getSystemService(Context.LOCATION_SERVICE);
            locationListener = new MyLocationListener();
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, 2L, 1, locationListener);
            if (!isConnected()){
                mainActivity.appendServer("Connecting...");
                connectToServer();
            } else {
                mainActivity.appendServer("Already Connected");
            }
        }
    }

    private void createJSONString(Location location){

        float altitude = (float) location.getAltitude();
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        float velocity = 10;
        float accuracy = location.getAccuracy();
        float bearing = location.getBearing();
        long time = (System.currentTimeMillis() / 1000L);

        mainActivity.appendServer("lat: " + latitude);
        mainActivity.appendServer("Long: " + longitude);


        JSONObject json = new JSONObject();
        JSONObject flightData = new JSONObject();
        try {
            json.put("type", "data");
            flightData.put("latitude", latitude);
            flightData.put("longitude", longitude);
            flightData.put("velocity", velocity);
            flightData.put("altitude", altitude);
            flightData.put("accuracy", accuracy);
            flightData.put("timeSent", time);
            json.put("flightData", flightData);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        final String message = json.toString();

        sendStringToServer(message);
    }

    private void sendStringToServer(final String message) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if(isConnected && webSocketClient != null)
                    webSocketClient.send(message);
            }
        }).start();
    }

    private void connectToServer() {

        URI uri;
        try {
            uri = new URI(WEB_SERVER_IP_PORT);
        } catch (URISyntaxException e) {
            mainActivity.appendServer("Error connecting to server: " + e.getMessage());
            return;
        }

        webSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen() {
                isConnected = true;
                updateConnectionStatus(ID);
            }

            @Override
            public void onTextReceived(String message) {

            }

            @Override
            public void onBinaryReceived(byte[] data) {

            }

            @Override
            public void onPingReceived(byte[] data) {

            }

            @Override
            public void onPongReceived(byte[] data) {

            }

            @Override
            public void onException(Exception e) {
                appendUiThread(e.getMessage(), ID);
                try{
                    webSocketClient.send("ping");
                }
                catch (Exception exception){
                    appendUiThread(exception.getMessage(), ID);
                    isConnected = false;
                }
            }

            @Override
            public void onCloseReceived() {
                isConnected = false;
                updateConnectionStatus(ID);
            }
        };

        webSocketClient.setConnectTimeout(10000);
        webSocketClient.setReadTimeout(60000);
        webSocketClient.addHeader("Origin", "http://developer.example.com");
        webSocketClient.enableAutomaticReconnection(3000);
        webSocketClient.connect();
    }

    private class MyLocationListener implements LocationListener{
        public static final int MY_PERMISSION_ACCESS_FINE_LOCATION = 0;
        @Override
        public void onLocationChanged(Location location) {
            createJSONString(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    }
}
